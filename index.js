const express = require("express");
const { appendFile } = require("fs");
const path = require("path");
const app = express();

app.get("/", function(request , response) {
    response.sendFile(__dirname + "/views/example-call.html");
})

const port = 8000;
app.listen(port, function() {
    console.log(`App running on port ${port}`);
})